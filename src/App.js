import React, { Component } from 'react';
import './App.css';
import chain from '../src/Helpers/chain';
import Listado from '../src/Components/Listado';

class App extends Component {

  constructor(props) {
    super(props);
    
  }

  state = {
    listado: []
  }


  componentDidMount() {
    let url = chain(2019);
    fetch(url)
      .then(res => res.json())
      .then(
        (result) => {
         
          this.setState({
            listado: result
          });
        },
        (error) => {
          // this.setState({
          //   isLoaded: true,
          //   error
          // });
        }
      )
  }
  
  render() {


    return (
      <div className="App">
        <header className="App-header">
         
          <p>
            Mi listado de feriados
          </p>
          
          <Listado feriados={this.state.listado}/>
         
        </header>
      </div>
    );
  }
}

export default App;
