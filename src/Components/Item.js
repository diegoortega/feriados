import React from 'react';

const Item = (props) => {
    return (<li>Día: {props.dia} - Motivo: {props.motivo} </li>);
}

export default Item;