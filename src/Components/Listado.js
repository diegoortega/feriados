import React, {Component} from 'react';
import Item from '../Components/Item';

class Listado extends Component{
    
    render()
    {
        var listado = [];

        if(this.props.feriados != null){
        
         for(var i=0; i<this.props.feriados.length; i++) {
                var lala = this.props.feriados[i];
                if (Object.keys(lala).length !== 0)
                {
                    Object.keys(lala).forEach(function(key) {
                       
                         var fecha = {
                                      mes: i,
                                      dia: key,
                                      motivo: lala[key].motivo,
                                      estado: lala[key].tipo
                                     };
                                     
                         listado.push(fecha);   
                    })
                }
         }
        
         var indice = 0;
         const listItems = listado.map((d) => <Item key={indice++} dia = {d.dia} motivo = {d.motivo} ></Item>);

         return (
            <ul>
                { listItems }
            </ul>
          )
        }
        else
        {
        return <p>"no se encontraron datos"</p>
        }

    }
}

export default Listado;